---
title: Specification post
date: 2019-10-17 10:36:43
tags:
---
## This is a personal reference for styling. 
## Please ignore this

Font spec:

#  Hello 1
##  Hello 2
###  Hello 3
#### Hello 4

General text

## And this is how you insert an image
![An Imgur Image](https://i.imgur.com/ucHPX7L.gif "An Imgur Image")

